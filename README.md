# Tangible AI Internship Project Report (Winter 2021 Cohort)

## Neural Machine Translation using PyTorch

This repo contains my final report and the code is included in the report.

The goal of this project is to build a neural machine translation model to translate between English and French phrases, to eventually translate less popular languages such as Amharic and Tigrinya. 

